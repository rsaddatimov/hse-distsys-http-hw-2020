import os
import json
import uuid
from flask import Flask, request

app = Flask(__name__)
db = {}
HTTP_OK = 200
HTTP_NOT_FOUND = 404


class Song(object):

    def __init__(self, name, author, id=None):
        self.name = name
        self.author = author
        self.id = id

    def to_json(self):
        return json.dumps({
            'name': self.name,
            'author': self.author,
            'id': self.id
        })


@app.route('/')
def hello_world():
    return 'Hello, World!'


@app.route('/publish')
def publish():
    try:
        data = json.loads(request.data)
        obj = Song(
            name=data['name'],
            author=data['author'],
            id=str(uuid.uuid4()).strip()
        )
        song_id = obj.id
    except:
        return 'Song has incorrect json!', HTTP_NOT_FOUND

    db[song_id] = obj
    return json.dumps({'id': song_id}), HTTP_OK


@app.route('/fetch')
def fetch():
    return json.dumps([obj.to_json() for obj in db.values()])


@app.route('/delete')
def delete():
    song_id = request.data.decode('utf-8').strip()

    if song_id not in db:
        return 'Song does not exist!', HTTP_NOT_FOUND

    db.pop(song_id)
    return 'Success', HTTP_OK


@app.route('/edit')
def edit():
    try:
        data = json.loads(request.data)
        song_id = data['id']

        if song_id not in db:
            return 'Song does not exist!', HTTP_NOT_FOUND

        obj = Song(
            name=data['name'],
            author=data['author'],
            id=song_id
        )
    except:
        return 'Incorrect data!', HTTP_NOT_FOUND

    db[song_id] = obj
    return 'Success', HTTP_OK


app.run(host='0.0.0.0', port=int(os.environ.get('HSE_HTTP_FLASK_PORT', 80)))
