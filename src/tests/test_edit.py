import os
import json
import requests


SERVER_HOST = os.environ.get('HSE_HTTP_TESTS_SERVER_HOST', 'server')
SERVER_PORT = int(os.environ.get('HSE_HTTP_FLASK_PORT', 80))
URL = 'http://' + SERVER_HOST
if SERVER_PORT != 80:
    URL += ':{}'.format(SERVER_PORT)


def init():
    data = json.loads(open('initial_songs.json', 'r').read())
    song_ids = []

    for song in data:
        resp = requests.get(URL + '/publish', data=json.dumps(song))

        assert resp.status_code == 200

        id = json.loads(resp.text)['id']
        song_ids.append(id)

    assert len(set(song_ids)) == len(data)

    return data, song_ids


def test_edit():
    data, ids = init()

    edited_song = {
        'name': data[0]['name'] + '_edited',
        'author': data[0]['author'] + '_edited',
        'id': ids[0]
    }

    resp = requests.get(URL + '/edit', data='error')
    assert resp.status_code == 404

    resp = requests.get(URL + '/edit', data=json.dumps(edited_song))
    assert resp.status_code == 200

    resp = requests.get(URL + '/fetch')
    assert resp.status_code == 200

    rec_data = json.loads(resp.text)
    for song in rec_data:
        name: str = song['name']
        author: str = song['author']

        if name.endswith('_edited'):
            assert author.endswith('_edited')
            assert song['id'] == ids[0]
            break
    else:
        assert False
