import os
import json
import requests


SERVER_HOST = os.environ.get('HSE_HTTP_TESTS_SERVER_HOST', 'server')
SERVER_PORT = int(os.environ.get('HSE_HTTP_FLASK_PORT', 80))
URL = 'http://' + SERVER_HOST
if SERVER_PORT != 80:
    URL += ':{}'.format(SERVER_PORT)


def init():
    data = json.loads(open('initial_songs.json', 'r').read())
    song_ids = []

    for song in data:
        resp = requests.get(URL + '/publish', data=json.dumps(song))

        assert resp.status_code == 200

        id = json.loads(resp.text)['id']
        song_ids.append(id)

    assert len(set(song_ids)) == len(data)

    return data, song_ids


def test_fetch():
    data, ids = init()

    resp = requests.get(URL + '/fetch')
    assert resp.status_code == 200

    rec_data = json.loads(resp.text)
    assert isinstance(rec_data, list)
    assert len(rec_data) == len(data)

    for song in rec_data:
        assert song['id'] in ids
        assert {
            'name': song['name'],
            'author': song['author']
        } in data
